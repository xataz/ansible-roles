
# Example
## group_vars or host_vars
```
proxmox:
  api_host: 192.168.1.250
  api_user: ansible@pve
  api_password: ansible_password
  validate_certs: no
  timeout: 60
  containers:
    defaults:
      cores: '1'
      cpus: '1'
      cpuunits: '1024'
      disk: '50'
      memory: '512'
      swap: '512'
      onboot: true
      unprivileged: true
      pubkey: 'ssh-ed25519 AAAA......'
      searchdomain: 'exemple.fr'
      storage: 'local'
      state: 'started'
      nameserver: '192.168.1.1'
      netmask: '24'
      ostemplate: 'local:vztmpl/debian-10.0-standard_10.0-1_amd64.tar.gz'
      features: "nesting=0,keyctl=0"
      pool: "ansible"
      node: 'proxmox'


proxmox_containers:
  lxc1:
    vmid: 140
    memory: '1024'
    swap: '1024'
    cores: '2'
    features: "nesting=1,keyctl=1"
    state: started
    mp:
      0: "/srv/data,mp=/storage"
  lxc2:
    vmid: 200
    memory: '128'
    swap: '0'
    features: 'fuse=1'
    ostemplate: 'local:vztmpl/alpine.3.12.tar.gz'
    netif: '{"net0":"name=eth0,gw=192.168.1.1,ip=192.168.1.200/24,bridge=vmbr0"}'
proxmox_vms:
  vm1:
    vmid: 140
    memory: '1024'
    cores: '2'
    state: started
  vm2:
    vmid: 200
    memory: '128'

```
