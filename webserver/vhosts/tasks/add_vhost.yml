- name: "Create vhost folder for {{ vhost_var.key }}"
  file:
    path: "/var/www/{{ vhost_var.key }}"
    state: "directory"
    owner: "www-data"
    group: "www-data"
    mode: "0755"
    recurse: true

- name: "Create certs folder for {{ vhost_var.key }}"
  file:
    path: "/etc/letsencrypt/live/{{ vhost_var.value.dns_names | first }}"
    state: "directory"
    owner: "www-data"
    group: "www-data"
    mode: "0755"
    recurse: true

- name: "Check if certificates exists for {{ vhost_var.key }}"
  stat:
    path: "/etc/letsencrypt/live/{{ vhost_var.value.dns_names | first }}/fullchain.pem"
  register: is_certificate_exist

- name: "Create temp vhost for {{ vhost_var.key }}"
  template:
    src: "templates/tmp.conf.j2"
    dest: "/etc/nginx/sites-enabled/{{ vhost_var.key }}.conf"
    owner: "www-data"
  vars:
    service_name: "{{ vhost_var.key }}"
    dns_names: "{{ vhost_var.value.dns_names }}"
    service_path: "{{ vhost_var.value.service_path | default('/') }}"
  register: new_tmp_vhost
  when: is_certificate_exist.stat.exists == false

- name: Restart nginx
  service:
    name: nginx
    state: restarted
  when: new_tmp_vhost is changed

- name: "Gen certificates for {{ vhost_var.key }}"
  command: >-
    ./acme.sh --issue -d {{ vhost_var.value.dns_names | join(" -d ") }}
    -w /var/www/{{ vhost_var.key }} 
  args:
    chdir: "~/.acme.sh"
  register: new_certs
  when: vhost_var.value.ssl == true
  changed_when: new_certs.rc == 0
  failed_when: new_certs.rc == 1

- name: "Install certs for {{ vhost_var.key }}"
  command: >-
    ./acme.sh --install-cert -d {{ vhost_var.value.dns_names | first }}
    --key-file /etc/letsencrypt/live/{{ vhost_var.value.dns_names | first }}/key.pem
    --fullchain-file /etc/letsencrypt/live/{{ vhost_var.value.dns_names | first }}/fullchain.pem
    --ca-file /etc/letsencrypt/live/{{ vhost_var.value.dns_names | first }}/ca.pem
  args:
    chdir: "~/.acme.sh"
  when: 
    - vhost_var.value.ssl == true
    - new_certs.rc != 1
    - is_certificate_exist.stat.exists == false

- name: "Create vhost for {{ vhost_var.key }}"
  template:
    src: "templates/vhost.conf.j2"
    dest: "/etc/nginx/sites-enabled/{{ vhost_var.key }}.conf"
    owner: "www-data"
  vars:
    service_name: "{{ vhost_var.key }}"
    dns_names: "{{ vhost_var.value.dns_names }}"
    php_enabled: "{{ vhost_var.value.php | default(false) }}"
    ssl_enabled: "{{ vhost_var.value.ssl | default(false) }}"
    service_path: "{{ vhost_var.value.service_path | default('/') }}"
  register: new_vhost
  notify: Restart nginx

- name: "Copy project"
  synchronize:
    src: "{{ awd }}/public/"
    dest: "/var/www/{{ vhost_var.key }}/"
    checksum: yes

- name: Chown project
  file:
    path: "/var/www/{{ vhost_var.key }}/"
    recurse: yes
    owner: www-data
    group: www-data
